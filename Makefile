.DEFAULT_GOAL := help
.PHONY: help

container: ## コンテナのビルド/実行
	docker-compose -f ../../github/docker-base/services/docker-network.yml -f docker-compose.yml up -d

container-destroy: ## コンテナの破棄
	docker-compose -f ../../github/docker-base/services/docker-network.yml -f docker-compose.yml stop
	docker-compose -f ../../github/docker-base/services/docker-network.yml -f docker-compose.yml rm -f

container-rebuild: ## コンテナの再作成＋イメージの再ビルド
	make container-destroy
	docker rmi go-dep-test-dev
	make container

container-rerun: ## コンテナの再作成
	make container-destroy
	make container

tty: ## コンテナに入る
	docker exec -it -u root -w /apps go-dep-test-dev bash

help: # [Makefileを自己文書化する](https://postd.cc/auto-documented-makefile/)
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
